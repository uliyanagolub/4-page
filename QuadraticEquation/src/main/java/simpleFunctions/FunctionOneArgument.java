/*интерфейс для понятия «функция одного вещественного аргумента,
определенная на отрезке [a; b]». */

package simpleFunctions;

public interface FunctionOneArgument {

    /* Интерфейс должен содержать метод вычисления значения
    функции при заданном аргументе*/
    double calculateFunction (double x);
    
    /* методы получения границ отрезка*/
    double getLeftLimit();
    double getRightLimit();
}
