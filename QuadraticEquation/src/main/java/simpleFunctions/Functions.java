package simpleFunctions;

abstract public class Functions implements FunctionOneArgument {
    private double leftLimit;
    private double rightLimit;

    public Functions(double leftLimit, double rightLimit) {
        if (leftLimit > rightLimit){
            throw new IllegalArgumentException("Incorrect limits");
        }
        this.leftLimit = leftLimit;
        this.rightLimit = rightLimit;
    }

    @Override
    public double getLeftLimit() {
        return leftLimit;
    }

    public void setLeftLimit(double leftLimit) {
        this.leftLimit = leftLimit;
    }

    @Override
    public double getRightLimit() {
        return rightLimit;
    }

    public void setRightLimit(double rightLimit) {
        this.rightLimit = rightLimit;
    }

    @Override
    abstract public double calculateFunction(double x) ;// {
//        return 0;
//    }

}
