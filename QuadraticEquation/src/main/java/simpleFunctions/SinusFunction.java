/*Функции вида f(x) = Asin(Bx)*/

package simpleFunctions;

public class SinusFunction  extends Functions {
    private double A, B;

    public SinusFunction(double a, double b, double A, double B) {
        super(a, b);
        this.A = A;
        this.B = B;
    }

    /* Филиппов А.В. 18.12.2020 Комментарий не удалять.
     Не работает.
     Где проверка отрезка определения функции?
    */
    @Override
    public double calculateFunction(double x){
        if(x < super.getLeftLimit() || x  > super.getRightLimit()){
            throw new IllegalArgumentException("x не в отрезке определения");
        }
        return A*Math.sin(B*x);
    }
}
