/*Aetrwbb dblf  f(x) = Aexp(x) + B*/

package simpleFunctions;

public class ExpFunction extends Functions {
    private double A, B;

    public ExpFunction(double a, double b, double A, double B) {
        super(a, b);
        this.A = A;
        this.B = B;
    }
    @Override
    public double calculateFunction(double x){
        if(x < super.getLeftLimit() || x  > super.getRightLimit()){
            throw new IllegalArgumentException("x не в отрезке определения");
        }
        return A*Math.exp(x) + B;
    }



}
