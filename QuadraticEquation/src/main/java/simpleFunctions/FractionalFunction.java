/*Функции вида f(x) = (Ax + B) / (Cx + D)*/

package simpleFunctions;

public class FractionalFunction extends Functions {
    private double A, B, C, D;

    public FractionalFunction(double a, double b, double A, double B, double C, double D) {
        super(a, b);
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;
    }
    @Override
    public double calculateFunction(double x){
        if(Math.abs(C * x + D ) <= 1e-6){
            throw new IllegalArgumentException("деление на ноль");
        }
        if(x < super.getLeftLimit() || x  > super.getRightLimit()){
            throw new IllegalArgumentException("x не в отрезке определения");
        }
        return (A*x + B)/(C*x + D);
    }
}
