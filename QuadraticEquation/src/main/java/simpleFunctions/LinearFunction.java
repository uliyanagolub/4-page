/*функции вида f(x) = Ax + B*/

package simpleFunctions;

public class LinearFunction extends Functions {
    private double coefA, coefB;

    public LinearFunction(double left, double right, double coefA, double coefB) {
        super(left, right);
        this.coefA = coefA;
        this.coefB = coefB;
    }

    @Override
    public double calculateFunction(double x){
        if(x < super.getLeftLimit() || x  > super.getRightLimit()){
            throw new IllegalArgumentException("x не в отрезке определения");
        }
        return coefA *x+ coefB;
    }
}
