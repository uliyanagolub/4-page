import QE.QuadraticEquation;
import QE.QuadraticEquationException;

public class QuadraticEquationWrapper {
    QuadraticEquation qe;

    public QuadraticEquationWrapper(QuadraticEquation qe){
        if(qe == null){
            throw new IllegalArgumentException("qe не должно быть null");
        }
        this.qe = qe;
    }

    public QuadraticEquationWrapper(double a,double b,double c) throws QuadraticEquationException {
        this(new QuadraticEquation(a, b, c));
    }

    double getBiggerRoot() throws QuadraticEquationException {
        double[] res = qe.roots();
        if(res.length > 1){
            return Math.max(res[0],res[1]);
        }
        return res[0];
    }
}
