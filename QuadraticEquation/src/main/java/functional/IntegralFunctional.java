package functional;

import simpleFunctions.FunctionOneArgument;

/* определенный интеграл на отрезке [a; b] */

public class IntegralFunctional<T extends FunctionOneArgument>  implements FunctionalOfOneArgument<T>{
    private double leftLimit;
    private double rightLimit;

/*пределы интегрирования хранятся как поля и устанавливаются конструктором*/

    public IntegralFunctional(double leftLimit, double rightLimit) {
        if (leftLimit > rightLimit){
        /*  если область определения функции не содержится в [a; b], то выбрасывается исключение */
            throw new IllegalArgumentException("Incorrect limits");
        }
        this.leftLimit = leftLimit;
        this.rightLimit = rightLimit;
    }

    public double getLeftLimit() {
        return leftLimit;
    }

    public void setLeftLimit(double leftLimit) {
        this.leftLimit = leftLimit;
    }

    public double getRightLimit() {
        return rightLimit;
    }

    public void setRightLimit(double rightLimit) {
        this.rightLimit = rightLimit;
    }


    /* Филиппов А.В. 18.12.2020 Комментарий не удалять.
     Не работает.
     Интеграл по значению функции в одной точке?
    */
    @Override
    public double functional(T function) {
        if (leftLimit < function.getLeftLimit() || rightLimit > function.getRightLimit()){
            throw new IllegalArgumentException("неверные границы интегрирования");
        }
        double integral = 0;
        int count = 10000000;
        for(int i = 1; i < count; ++i){
            try {
                integral += function.calculateFunction( leftLimit +( i * (rightLimit - leftLimit) / ((double) count)));
            } catch (IllegalArgumentException e) {
                continue;
            }
        }
        return integral * ((rightLimit - leftLimit) / (double) count);
    }

}
