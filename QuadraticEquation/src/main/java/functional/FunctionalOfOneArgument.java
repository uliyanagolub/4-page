/* параметризованный интерфейс
«Функционал от одного аргумента» c методом вычисления значения функционала от
заданной функции */

package functional;

import simpleFunctions.FunctionOneArgument;

public interface FunctionalOfOneArgument<T extends FunctionOneArgument> {
    double functional(T function);
}
