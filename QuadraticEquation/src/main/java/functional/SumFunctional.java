package functional;

import simpleFunctions.FunctionOneArgument;

/*сумма значений функции на концах отрезка и в его середине*/

public class SumFunctional<T extends FunctionOneArgument> implements FunctionalOfOneArgument<T> {

    @Override
    public double functional(T function) {
        return (function.calculateFunction(function.getLeftLimit()) +
                function.calculateFunction(function.getRightLimit()) +
                function.calculateFunction((function.getRightLimit() - function.getLeftLimit()) / 2));
    }
}
