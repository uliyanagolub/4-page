
import org.junit.Test;
import functional.IntegralFunctional;
import functional.SumFunctional;
import simpleFunctions.LinearFunction;
import simpleFunctions.SinusFunction;

import static org.junit.Assert.assertEquals;

public class TestFunctional {
    private static  LinearFunction linearFunction = new LinearFunction(0, 5, 2,1);

    @Test
    public void sumFunctional(){
        SumFunctional<LinearFunction> sumFunctional = new SumFunctional<>();
        assertEquals(18, sumFunctional.functional(linearFunction), 10^(-8));
    }

    @Test
    public void integralFunctional1(){
        IntegralFunctional<LinearFunction> integralFunctional = new IntegralFunctional<>(1, 4);
        assertEquals(18, integralFunctional.functional(linearFunction), 1e-5);
    }

    /* Филиппов А.В. 18.12.2020 Комментарий не удалять.
     тест, который должен работать
    */
    @Test
    public void integralFunctional3(){
        SinusFunction sin = new SinusFunction(-100, 100, 1, 1);
        IntegralFunctional<SinusFunction> integralFunctional = new IntegralFunctional<>(0, Math.PI);
        assertEquals(2, integralFunctional.functional(sin), 1e-8);
    }

    @Test (expected = IllegalArgumentException.class)
    public void integralFunctional2(){
        IntegralFunctional<LinearFunction> integralFunctional1 = new IntegralFunctional<>(-1, 7);
        integralFunctional1.functional(linearFunction);
    }

}
