import org.junit.Assert;
import org.junit.Test;
import simpleFunctions.ExpFunction;
import simpleFunctions.FractionalFunction;
import simpleFunctions.LinearFunction;
import simpleFunctions.SinusFunction;

public class TestFunctions {
    @Test
    public void expFunction(){
        ExpFunction expFunction = new ExpFunction(1,2,3,4);
        Assert.assertEquals(7, expFunction.calculateFunction(0), 10^(-8));
    }
    @Test
    public void fractionalFunction(){
        FractionalFunction fractionalFunction = new FractionalFunction(0, 5, 2, 8, 4,6);
        Assert.assertEquals(1, fractionalFunction.calculateFunction(1), 10^(-8));
    }
    @Test
    public  void linearFunction(){
        LinearFunction linearFunction = new LinearFunction(0, 5, 4, 2);
        Assert.assertEquals(6, linearFunction.calculateFunction(1), 10^(-8));
    }
    @Test
    public void sinusFunction(){
        SinusFunction sinusFunction = new SinusFunction(0, 5, 2,4);
        Assert.assertEquals(0, sinusFunction.calculateFunction(0), 10^(-8));
    }

    /* Филиппов А.В. 18.12.2020 Комментарий не удалять.
     Не работает. Вычисление функции вне отрезка, должно вызывать исключение.
    */
    @Test(expected = IllegalArgumentException.class)
    public void sinusFunction2(){
        SinusFunction sinusFunction = new SinusFunction(0, 5, 2,4);
        sinusFunction.calculateFunction(Math.PI * 10);
    }
}
