import QE.QuadraticEquation;
import QE.QuadraticEquationException;

import org.junit.Test;
import static org.junit.Assert.*;
public class QuadTest {
    @Test
    public void testRoots() throws QuadraticEquationException {
        QuadraticEquation quadraticEquation1 = new QuadraticEquation(1.0, 2.0, -48.0);
        QuadraticEquation quadraticEquation2 = new QuadraticEquation(4.0, -4.0, 1);
        QuadraticEquationWrapper qew1 = new QuadraticEquationWrapper(quadraticEquation1);
        QuadraticEquationWrapper qew2 = new QuadraticEquationWrapper(quadraticEquation2);
        assertEquals(6.0, qew1.getBiggerRoot(), 1e-6);;
        assertEquals(0.5, qew2.getBiggerRoot(), 1e-6);
    }

    @Test
    public  void testNoRoots() {
        try {
            QuadraticEquationWrapper qew = new QuadraticEquationWrapper(new QuadraticEquation(1.0, 2.0, 5.0));
            qew.getBiggerRoot();
        } catch (QuadraticEquationException e) {
            if (!"No roots".equals(e.getErrorCode().toString())) {
                fail();
            }
        }
    }
}