/* В нем создайте класс для хранения квадратного трехчлена с
методом решения соответствующего квадратного уравнения (параметров нет, а
результат — массив корней)*/

/* Филиппов А.В. 18.12.2020 Комментарий не удалять.
 Не работает. А где maven проект, который зависит от этого проекта?
*/
public class QuadraticEquation {
    private double a;
    private double b;
    private double c;

    public QuadraticEquation(double a, double b, double c) throws QuadraticEquationException {
        if (Double.compare(a,0) == 0) {
            throw new QuadraticEquationException(QuadraticEquationErrorCode.NOT_A_QUADRATIC_EQUATION);
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }


    public double [] roots() throws QuadraticEquationException {
        double[] roots;
        double d = b * b - 4 * a * c;
        if (d > 0) {
            double x1 = (-b + Math.sqrt(d)) / (2 * a);
            double x2 = (-b - Math.sqrt(d)) / (2 * a);
            roots = new double[]{x1, x2};
        } else if (Double.compare(d,0) == 0) {
            double x = (-b + Math.sqrt(d)) / (2 * a);
            roots = new double[]{x};
        } else {
            throw new QuadraticEquationException(QuadraticEquationErrorCode.NO_ROOTS);
        }
        return roots;
    }
}
