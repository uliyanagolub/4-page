public enum QuadraticEquationErrorCode {
    NOT_A_QUADRATIC_EQUATION("Not a quadratic equation"),
    NO_ROOTS("No roots");

    String string;

    QuadraticEquationErrorCode(String string) {
        this.string = string;
    }
    public String toString() {
        return string;
    }
}
