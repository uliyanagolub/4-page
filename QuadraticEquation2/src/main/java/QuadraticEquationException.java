public class QuadraticEquationException extends Exception {
    QuadraticEquationErrorCode errorCode;
    public QuadraticEquationException(QuadraticEquationErrorCode errorCode){
        this.errorCode = errorCode;
    }

    public QuadraticEquationErrorCode getErrorCode() {
        return errorCode;
    }
}
