import QE.QuadraticEquation;
import QE.QuadraticEquationException;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestQuadraticEquation {
    @Test
    public void testRoots() throws QuadraticEquationException {
        QuadraticEquation quadraticEquation1 = new QuadraticEquation(1.0, 2.0, -48.0);
        QuadraticEquation quadraticEquation2 = new QuadraticEquation(4.0, -4.0, 1);
        assertArrayEquals(new double[]{6.0, -8.0}, quadraticEquation1.roots(), 1e-6);
        assertArrayEquals (new double[]{0.5}, quadraticEquation2.roots(), 1e-6);
    }
    @Test
    public  void testException() {
        try {
            new QuadraticEquation(0, 2, 1);
        } catch (QuadraticEquationException e) {
            if (!"Not a quadratic equation".equals(e.getErrorCode().toString())) {
                fail();
            }
        }
    }

    @Test
    public  void testNoRoots() {
        try {
            QuadraticEquation quadraticEquation3 = new QuadraticEquation(1.0, 2.0, 5.0);
            quadraticEquation3.roots();
        } catch (QuadraticEquationException e) {
            if (!"No roots".equals(e.getErrorCode().toString())) {
                fail();
            }
        }
    }
}
